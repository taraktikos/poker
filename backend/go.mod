module pok

go 1.12

require (
	github.com/coreos/bbolt v1.3.2
	github.com/ernestosuarez/itertools v0.0.0-20160926002703-030494752e01
	github.com/google/uuid v1.1.0
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.3.0
)

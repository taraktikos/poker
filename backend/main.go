package main

import (
	"fmt"
	"github.com/pkg/errors"
	"log"
	"math/rand"
	"pok/holdem"
	"pok/pokerodds"
	"sort"
)

func main() {
	storage := holdem.NewMapStorage()
	algorithms := []holdem.Algorithm{
		//{
		//	Name: "Random",
		//	RoundFuncs: map[int]holdem.RoundFunc{
		//		holdem.DEAL:    RandomAlgorithm,
		//		holdem.PREFLOP: RandomAlgorithm,
		//		holdem.FLOP:    RandomAlgorithm,
		//		//third.TURN:    RandomAlgorithm,
		//	},
		//	InitialBalance: 100,
		//},
		//{
		//	Name: "HandProbabilityAnalyses",
		//	RoundFuncs: map[int]holdem.RoundFunc{
		//		holdem.FLOP: HandProbabilityAnalyses,
		//	},
		//	InitialBalance: 100,
		//},
		{
			Name: "HandProbabilityAnalysesLay",
			RoundFuncs: map[int]holdem.RoundFunc{
				holdem.FLOP: HandProbabilityAnalysesLay,
				//holdem.TURN: HandProbabilityAnalysesLayStep2,
			},
			InitialBalance: 100,
		},
	}
	pockerGame := holdem.NewPockerGame(algorithms, storage)

	pockerGame.RetrieveGames()

	pockerGame.ProcessHistory()

	pockerGame.PlacingBets()
}

func RandomAlgorithm(game holdem.Game, storage holdem.Storage) (hand holdem.Hand, err error) {
	bets := storage.GetBetsByMarketAndAlgorithm(game.Markets.Markets[0].ID, "Random")
	for _, b := range bets {
		if b.Round == game.Round {
			return hand, errors.New(fmt.Sprintf("bet was already placed in round %v", game.Round))
		}
	}

	hand = holdem.Hand{
		Index:  rand.Intn(3),
		Amount: 1.0,
		Side:   holdem.BACK,
	}
	return hand, nil
}

func getMinProbabilitiesWithSelection(probabilities []float64, market holdem.Market) (handIndex int, minProbability float64) {
	handIndex = 0
	minProbability = probabilities[0]
	for i, prob := range probabilities {
		if len(market.Selections.Selections[i].BestAvailableToLayPrices.Prices) == 0 {
			log.Printf("[WARN][%s] Hand %v alredy lost. skiping...", i+1)
			continue
		}
		if prob < minProbability {
			handIndex = i
			minProbability = prob
		}
	}
	return handIndex, minProbability
}

type handWithPriceAndProb struct {
	handIndex   int
	probability float64
	backPrice   float64
	layPrice    float64
}

func getThirdMinProbabilitiesWithSelection(probabilities []float64) []handWithPriceAndProb {
	var hands []handWithPriceAndProb
	for i, prob := range probabilities {
		hands = append(hands, handWithPriceAndProb{
			handIndex:   i,
			probability: prob,
		})
	}
	return hands
}

func HandProbabilityAnalysesLay(game holdem.Game, storage holdem.Storage) (hand holdem.Hand, err error) {
	name := "HandProbabilityAnalysesLay"
	bets := storage.GetBetsByMarketAndAlgorithmAndRound(game.Markets.Markets[0].ID, name, game.Round)
	selections := game.Markets.Markets[0].Selections.Selections
	if len(bets) > 0 {
		return hand, errors.New(fmt.Sprintf("bet already placed on this market for algorithm %v", name))
	}

	hands := holdem.ParseHands(game.GameData)
	board := holdem.ParseBoard(game.GameData)

	calc := pokerodds.New()
	log.Printf("[INFO][%s] analyse hands %v %v", name, hands, board)
	probabilities := calc.Calculate(hands, board)[1:]
	printProbabilitiesAndPrices(selections, name, probabilities)
	handWithProb := getThirdMinProbabilitiesWithSelection(probabilities)
	sort.Slice(handWithProb, func(i, j int) bool {
		return handWithProb[i].probability > handWithProb[j].probability
	})
	selectedThirdHand := handWithProb[3]
	log.Printf("[INFO][%s] Select %s with probabilities %v", name, game.GameData.Objects[selectedThirdHand.handIndex].Name, selectedThirdHand.probability)

	coef := 0.0
	if len(selections[selectedThirdHand.handIndex].BestAvailableToLayPrices.Prices) > 0 {
		coef = selections[selectedThirdHand.handIndex].BestAvailableToLayPrices.Prices[0].Value
	}
	if coef == 0 {
		return hand, errors.New("hand already terminated skipping...")
	}
	if coef >= 5 {
		return hand, errors.New("coefficient > 4.9 skipping...")
	}
	if containsHighestCard(hands[selectedThirdHand.handIndex]) {
		return hand, errors.New(fmt.Sprintf("hand %v contains highest card. skipping", hands[selectedThirdHand.handIndex]))
	}

	hand = holdem.Hand{
		Index:  selectedThirdHand.handIndex,
		Amount: 1.0,
		Side:   holdem.LAY,
	}

	return hand, nil
}

func containsHighestCard(hand pokerodds.Hand) bool {
	v1 := pokerodds.Values[string(hand[0][0])]
	//suit1 := string(hand[0][1])
	v2 := pokerodds.Values[string(hand[1][0])]
	//suit1 := string(hand[1][1])
	return v1 > 9 || v2 > 9
}

func printProbabilitiesAndPrices(selections []holdem.Selection, name string, probabilities []float64) {
	for i := 0; i < 4; i++ {
		backPrice := 0.0
		layPrice := 0.0
		if len(selections[i].BestAvailableToBackPrices.Prices) > 0 {
			backPrice = selections[i].BestAvailableToBackPrices.Prices[0].Value
		}
		if len(selections[i].BestAvailableToLayPrices.Prices) > 0 {
			layPrice = selections[i].BestAvailableToLayPrices.Prices[0].Value
		}
		log.Printf("[INFO][%s] Hand %v: %v - %v | %v", name, i+1, probabilities[i], backPrice, layPrice)
	}
}

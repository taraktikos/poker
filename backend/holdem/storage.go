package holdem

import (
	"strings"
	"sync"
)

type Storage interface {
	AddBet(bet Bet)
	UpdateBet(marketID int, bet Bet)
	GetBetsByMarket(marketID int) []Bet
	GetBetsByMarketAndAlgorithm(marketID int, algorithm string) []Bet
	GetBetsByMarketAndAlgorithmAndRound(marketID int, algorithm string, round int) []Bet
}

type mapStorage struct {
	bets map[int][]Bet
	lock sync.RWMutex
}

func NewMapStorage() *mapStorage {
	return &mapStorage{
		bets: make(map[int][]Bet),
	}
}

func (m *mapStorage) AddBet(bet Bet) {
	m.lock.Lock()
	defer m.lock.Unlock()
	m.bets[bet.MarketID] = append(m.bets[bet.MarketID], bet)
}

func (m *mapStorage) UpdateBet(marketID int, bet Bet) {
	bets := m.GetBetsByMarket(marketID)
	m.lock.Lock()
	defer m.lock.Unlock()
	for i, b := range bets {
		if bet.ID == b.ID {
			m.bets[marketID][i] = bet
			break
		}
	}
}

func (m *mapStorage) GetBetsByMarket(marketID int) []Bet {
	m.lock.RLock()
	defer m.lock.RUnlock()
	bets, ok := m.bets[marketID]
	if !ok {
		return []Bet{}
	}
	return bets
}

func (m *mapStorage) GetBetsByMarketAndAlgorithm(marketID int, algorithm string) []Bet {
	m.lock.RLock()
	defer m.lock.RUnlock()
	bets, ok := m.bets[marketID]
	if !ok {
		return []Bet{}
	}
	var filteredBets []Bet
	for _, bet := range bets {
		if strings.Compare(bet.Algorithm, algorithm) == 0 {
			filteredBets = append(filteredBets, bet)
		}
	}
	return filteredBets
}

func (m *mapStorage) GetBetsByMarketAndAlgorithmAndRound(marketID int, algorithm string, round int) (filteredBets []Bet) {
	m.lock.RLock()
	defer m.lock.RUnlock()
	bets, ok := m.bets[marketID]
	if !ok {
		return []Bet{}
	}

	for _, bet := range bets {
		if strings.Compare(bet.Algorithm, algorithm) == 0 && bet.Round == round {
			filteredBets = append(filteredBets, bet)
		}
	}
	return filteredBets
}

//func NewBoltDB(fileName string, options bolt.Options) (*boltDB, error) {
//	log.Printf("[INFO] create boltdb store %s, %+v", fileName, options)
//	db, err := bolt.Open(fileName, 0600, &options)
//	if err != nil {
//		return nil, errors.Wrapf(err, "failed to make boltdb for %s", fileName)
//	}
//	buckets := []string{betsBucketName}
//	err = db.Update(func(tx *bolt.Tx) error {
//		for _, name := range buckets {
//			if _, e := tx.CreateBucketIfNotExists([]byte(name)); e != nil {
//				return errors.Wrapf(err, "failed to create top level bucket %s", name)
//			}
//		}
//		return nil
//	})
//	if err != nil {
//		return nil, errors.Wrap(err, "failed to create top level bucket")
//	}
//	return &boltDB{db}, nil
//}

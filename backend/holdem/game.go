package holdem

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"io/ioutil"
	"log"
	"net/http"
	"pok/pokerodds"
	"strings"
	"sync"
	"time"
)

type RoundFunc func(game Game, storage Storage) (Hand, error)

type Algorithm struct {
	Name           string
	RoundFuncs     map[int]RoundFunc
	InitialBalance float64
}

type Poker struct {
	games            chan Channel
	balances         map[string]float64
	processedMarkets map[int]bool
	algorithms       []Algorithm
	storage          Storage
	lock             sync.Mutex
}

type Hand struct {
	Index          int
	Amount         float64
	Side           BetSide
	WinProbability float64
}

type Bet struct {
	ID       string
	GameID   int
	MarketID int
	Side     BetSide
	Price    float64
	Size     float64
	Round    int
	Profit   float64
	Hand     struct {
		Name           string
		SelectionId    int
		HandIndex      int
		WinProbability float64
	}
	Status    BetStatus
	Algorithm string
}

func NewPockerGame(algorithms []Algorithm, storage Storage) *Poker {
	balances := make(map[string]float64)
	for _, a := range algorithms {
		balances[a.Name] = a.InitialBalance
	}
	return &Poker{
		algorithms:       algorithms,
		storage:          storage,
		games:            make(chan Channel),
		balances:         balances,
		processedMarkets: make(map[int]bool),
	}
}

type BetStatus string

const (
	WON  BetStatus = "WON"
	LOST BetStatus = "LOST"
)

type BetSide string

const (
	BACK BetSide = "BACK"
	LAY  BetSide = "LAY"
)

const (
	DEAL    = 1
	PREFLOP = 2
	FLOP    = 3
	TURN    = 4
	RIVER   = 5
)

const ChannelSnapshotUrl = "https://api.games.betfair.com/rest/v1/channels/1444080/snapshot?type=json"
const ChannelHistoryUrl = "https://api.games.betfair.com/rest/v1/channels/1444080/history?type=json"

func (p *Poker) PlacingBets() {
	for game := range p.games {
		if game.Game.Round == RIVER {
			log.Printf("[INFO] winner detection skipping...")
			continue
		}
		for _, algorithm := range p.algorithms {
			if roundFunc, ok := algorithm.RoundFuncs[game.Game.Round]; ok {
				hand, err := roundFunc(game.Game, p.storage)
				if err != nil {
					log.Printf("[ERROR] error happened during selecting hand: %v", err)
					continue
				}
				err = p.PlaceBet(game.Game, hand, algorithm.Name)
				if err != nil {
					log.Printf("[ERROR] error happened during placing bet: %v", err)
				}
			}
		}
	}
}

func (p *Poker) RetrieveGames() {
	ticker := time.NewTicker(time.Second * 1).C
	go func() {
		for {
			select {
			case <-ticker:
				channelResponse, err := fetchChannel(ChannelSnapshotUrl)
				if err != nil {
					log.Printf("[ERROR] can't fetch channel: %+v", err)
					continue
				}

				if !isRunning(channelResponse) {
					//log.Printf("[DEBUG] channel is not running. Skipping...")
					continue
				}

				p.games <- channelResponse.Channel
			}
		}
	}()
}

func (p *Poker) ProcessHistory() {
	ticker := time.NewTicker(time.Second * 1).C
	go func() {
		for {
			select {
			case <-ticker:
				historyResponse, err := fetchHistory(ChannelHistoryUrl)
				if err != nil {
					log.Printf("[ERROR] can't fetch history: %+v", err)
					continue
				}

				for _, game := range historyResponse.Channel.Games.Games {
					market := game.Markets.Markets[0]
					//if !isStatusSettled(&market) {
					//	log.Printf("[WARN] market is not settled. Skipping")
					//	continue
					//}
					if _, ok := p.processedMarkets[market.ID]; ok {
						continue
					}
					log.Printf("[INFO] game finished %v %v", ParseHands(game.GameData), ParseBoard(game.GameData))
					if len(p.storage.GetBetsByMarket(market.ID)) == 0 {
						log.Printf("[DEBUG] no bets placed on market %v", market.ID)
						p.processedMarkets[market.ID] = true
						continue
					}
					bets := p.storage.GetBetsByMarket(market.ID)
					for _, bet := range bets {
						p.settleBet(&bet, market)
						p.storage.UpdateBet(market.ID, bet)
					}
					p.processedMarkets[market.ID] = true
				}
			}
		}
	}()
}

func ParseHands(game GameData) (hs pokerodds.Hands) {
	for i, obj := range game.Objects {
		if i >= 4 {
			break
		}
		hs = append(hs, []string{cardMapping[obj.Properties[0].Value], cardMapping[obj.Properties[1].Value]})
	}
	return hs
}

func ParseBoard(game GameData) (board []string) {
	for _, v := range game.Objects[4].Properties {
		if strings.Compare(v.Value, "NOT AVAILABLE") == 0 {
			continue
		}
		board = append(board, cardMapping[v.Value])
	}
	return board
}

func isBetWin(bet *Bet, market HistoryMarket) bool {
	handStatus := market.Selections.Selections[bet.Hand.HandIndex].Status
	if bet.Side == BACK {
		return strings.Compare(handStatus, "WINNER") == 0
	}
	return strings.Compare(handStatus, "LOSER") == 0
}

func (p *Poker) settleBet(bet *Bet, market HistoryMarket) {
	p.lock.Lock()
	defer p.lock.Unlock()

	isBetWin := isBetWin(bet, market)
	if isBetWin && bet.Side == BACK {
		bet.Status = WON
		bet.Profit = (bet.Size * bet.Price) - bet.Size
		returnSize := bet.Size
		p.balances[bet.Algorithm] = p.balances[bet.Algorithm] + returnSize + bet.Profit
	}

	if isBetWin && bet.Side == LAY {
		bet.Status = WON
		bet.Profit = bet.Size
		returnSize := bet.Size * bet.Price
		p.balances[bet.Algorithm] = p.balances[bet.Algorithm] + returnSize + bet.Profit
	}

	if !isBetWin && bet.Side == BACK {
		bet.Status = LOST
		bet.Profit = -bet.Size
	}

	if !isBetWin && bet.Side == LAY {
		bet.Status = LOST
		bet.Profit = -bet.Size * bet.Price
	}
	log.Printf("[INFO] bet %v %v", bet.Status, bet)
	log.Printf("[INFO] new balance %v", p.balances)
}

func (p *Poker) PlaceBet(game Game, hand Hand, algorithmName string) error {
	selections := game.Markets.Markets[0].Selections.Selections
	backPrices := selections[hand.Index].BestAvailableToBackPrices.Prices
	layPrices := selections[hand.Index].BestAvailableToLayPrices.Prices
	if hand.Side == BACK && len(backPrices) == 0 {
		return errors.New("prices for back is not available")
	}
	if hand.Side == LAY && len(layPrices) == 0 {
		return errors.New("prices for lay is not available")
	}
	price := 0.0
	amountForDeduct := 0.0
	if hand.Side == BACK {
		price = backPrices[0].Value
		amountForDeduct = hand.Amount
	} else {
		price = layPrices[0].Value
		amountForDeduct = hand.Amount * price
	}
	bet := Bet{
		ID:        uuid.New().String(),
		GameID:    game.ID,
		MarketID:  game.Markets.Markets[0].ID,
		Algorithm: algorithmName,
		Side:      hand.Side,
		Price:     price,
		Size:      hand.Amount,
		Round:     game.Round,
		Hand: struct {
			Name           string
			SelectionId    int
			HandIndex      int
			WinProbability float64
		}{
			Name: game.GameData.Objects[hand.Index].Name, SelectionId: selections[hand.Index].ID, HandIndex: hand.Index, WinProbability: hand.WinProbability,
		},
	}
	p.storage.AddBet(bet)

	p.lock.Lock()
	defer p.lock.Unlock()
	p.balances[algorithmName] = p.balances[algorithmName] - amountForDeduct
	log.Printf("[INFO] Bet placed: %+v", bet)
	log.Printf("[INFO] Balances: %+v", p.balances)

	return nil
}

func isRunning(channel *ChannelResponse) bool {
	market := channel.Channel.Game.Markets.Markets[0]
	return strings.Compare(channel.Channel.Status, "RUNNING") == 0 && strings.Compare(market.Status, "ACTIVE") == 0
}

func isStatusSettled(market *HistoryMarket) bool {
	return strings.Compare(market.Status, "SETTLED") == 0
}

type Selection struct {
	Name     string `json:"name"`
	Resource struct {
		Href         string `json:"href"`
		Title        string `json:"title"`
		ResponseType string `json:"responseType"`
	} `json:"resource"`
	Status                    string  `json:"status"`
	AmountMatched             float64 `json:"amountMatched"`
	BestAvailableToBackPrices struct {
		Prices []struct {
			Value           float64 `json:"value"`
			AmountUnmatched float64 `json:"amountUnmatched"`
		} `json:"prices"`
	} `json:"bestAvailableToBackPrices"`
	BestAvailableToLayPrices struct {
		Prices []struct {
			Value           float64 `json:"value"`
			AmountUnmatched float64 `json:"amountUnmatched"`
		} `json:"prices"`
	} `json:"bestAvailableToLayPrices"`
	ID int `json:"id"`
}

type Market struct {
	Status         string  `json:"status"`
	CommissionRate float64 `json:"commissionRate"`
	MarketType     string  `json:"marketType"`
	Selections     struct {
		Selections []Selection `json:"selections"`
		Type       string      `json:"type"`
	} `json:"selections"`
	ID     int `json:"id"`
	NextID int `json:"nextId"`
}

type Markets []struct {
	Status     string `json:"status"`
	MarketType string `json:"marketType"`
	Selections struct {
		Selections []struct {
			Name   string `json:"name"`
			Status string `json:"status"`
		} `json:"selections"`
		Type string `json:"type"`
	} `json:"selections"`
	ID int `json:"id"`
}

type Game struct {
	Round                           int      `json:"round"`
	BettingWindowTime               int      `json:"bettingWindowTime"`
	BettingWindowPercentageComplete int      `json:"bettingWindowPercentageComplete"`
	GameData                        GameData `json:"gameData"`
	Markets                         struct {
		Markets  []Market `json:"markets"`
		Currency string   `json:"currency"`
	} `json:"markets"`
	ID int `json:"id"`
}

type Channel struct {
	Status   string `json:"status"`
	Game     Game   `json:"game"`
	ID       int    `json:"id"`
	Name     string `json:"name"`
	GameType string `json:"gameType"`
}

type ChannelResponse struct {
	Channel Channel `json:"channel"`
}

type GameData struct {
	Objects []struct {
		Description string `json:"description"`
		Status      string `json:"status"`
		Properties  []struct {
			Name  string `json:"name"`
			Value string `json:"value"`
		} `json:"properties"`
		Name string `json:"name"`
	} `json:"objects"`
}

type HistoryResponse struct {
	Channel struct {
		Games struct {
			NextPage struct {
				Resource struct {
					Href         string `json:"href"`
					Title        string `json:"title"`
					ResponseType string `json:"responseType"`
				} `json:"resource"`
			} `json:"nextPage"`
			Games []struct {
				GameData GameData `json:"gameData"`
				Markets  struct {
					Markets  []HistoryMarket `json:"markets"`
					Currency string          `json:"currency"`
				} `json:"markets"`
				GameStartDate time.Time `json:"gameStartDate"`
				ID            int       `json:"id"`
			} `json:"games"`
			Total int `json:"total"`
			Start int `json:"start"`
			End   int `json:"end"`
		} `json:"games"`
		ID       int    `json:"id"`
		Name     string `json:"name"`
		GameType string `json:"gameType"`
	} `json:"channel"`
}

type HistoryMarket struct {
	Status     string `json:"status"`
	MarketType string `json:"marketType"`
	Selections struct {
		Selections []struct {
			Name   string `json:"name"`
			Status string `json:"status"`
		} `json:"selections"`
		Type string `json:"type"`
	} `json:"selections"`
	ID int `json:"id"`
}

func fetchChannel(url string) (*ChannelResponse, error) {
	req, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer req.Body.Close()

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return nil, err
	}

	var resp ChannelResponse
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}
	return &resp, nil
}

func fetchHistory(url string) (*HistoryResponse, error) {
	req, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer req.Body.Close()

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return nil, err
	}

	var resp HistoryResponse
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}
	return &resp, nil
}

var cardMapping = map[string]string{
	//clubs
	"0":  "Ac",
	"1":  "2c",
	"2":  "3c",
	"3":  "4c",
	"4":  "5c",
	"5":  "6c",
	"6":  "7c",
	"7":  "8c",
	"8":  "9c",
	"9":  "Tc",
	"10": "Jc",
	"11": "Qc",
	"12": "Kc",
	//diamonds
	"13": "Ad",
	"14": "2d",
	"15": "3d",
	"16": "4d",
	"17": "5d",
	"18": "6d",
	"19": "7d",
	"20": "8d",
	"21": "9d",
	"22": "Td",
	"23": "Jd",
	"24": "Qd",
	"25": "Kd",
	//hearts
	"26": "Ah",
	"27": "2h",
	"28": "3h",
	"29": "4h",
	"30": "5h",
	"31": "6h",
	"32": "7h",
	"33": "8h",
	"34": "9h",
	"35": "Th",
	"36": "Jh",
	"37": "Qh",
	"38": "Kh",
	//spades
	"39": "As",
	"40": "2s",
	"41": "3s",
	"42": "4s",
	"43": "5s",
	"44": "6s",
	"45": "7s",
	"46": "8s",
	"47": "9s",
	"48": "Ts",
	"49": "Js",
	"50": "Qs",
	"51": "Ks",
}

package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"sync"
	"time"
)



type bet struct {
	gameId int
	marketId int
	side string
	price float64
	size float64
	hand struct{
		name string
		selectionId int
		num int
	}
}

var bal = 10.12
var placedBets = make(map[int]bet)
var checkedBets = make(map[int]bool)

func main()  {
	var wg sync.WaitGroup
	ticker := time.NewTicker(time.Second * 2).C
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case <-ticker:
				resp, err := getRep("https://api.games.betfair.com/rest/v1/channels/1444080/snapshot?type=json")
				if err != nil {
					log.Printf("%+v", err)
				}
				market := resp.Channel.Game.Markets.Markets[0]

				if strings.Compare(resp.Channel.Status, "RUNNING") == 0 {
					if resp.Channel.Game.Round == 4 {
						if resp.Channel.Game.BettingWindowPercentageComplete > 0 {
							if _, ok := placedBets[market.ID]; ok {
								log.Printf("bet already placed")
								continue
							}
							prices := market.Selections.Selections[0].BestAvailableToBackPrices.Prices
							if len(prices) == 0 {
								log.Printf("prices not avaliable. skip")
								continue
							}
							n := 0
							lowestPrice := prices[n].Value
							for i, s := range market.Selections.Selections {
								if len(s.BestAvailableToBackPrices.Prices) > 0 && s.BestAvailableToBackPrices.Prices[0].Value < lowestPrice {
									lowestPrice = s.BestAvailableToBackPrices.Prices[0].Value
									n = i
								}
							}
							sel := market.Selections.Selections[n]
							b := bet {
								gameId:resp.Channel.Game.ID,
								marketId:market.ID,
								side:"back",
								price:sel.BestAvailableToBackPrices.Prices[0].Value,
								size:1.0,
								hand: struct {
									name        string
									selectionId int
									num         int
								}{
									name: resp.Channel.Game.GameData.Objects[n].Name, selectionId: sel.ID, num: n},
							}
							placedBets[market.ID] = b
							log.Printf("placed bet %+v", b)
						}
					}

					if resp.Channel.Game.Round == 5 {
						if _, ok := checkedBets[market.ID]; ok {
							continue
						}
						b, ok := placedBets[market.ID]
						if !ok {
							log.Printf("bet was not placed.")
							continue
						}
						sel := market.Selections.Selections[b.hand.num]
						log.Printf("bet was placed %+v", b)
						log.Printf("selection %+v", sel)
						if strings.Compare(sel.Status, "LOSER") == 0 {
							bal -= b.size
							log.Printf("You lose %+v", b.size)
						} else if strings.Compare(sel.Status, "WINNER") == 0 {
							profit := (b.size * b.price) - b.size
							bal += profit
							log.Printf("You win %+v", profit)
						} else {
							log.Printf("noname status %+v", sel.Status)
						}
						checkedBets[market.ID] = true
						log.Printf("Balance = %+v", bal)
					}
				}
			}
		}

	}()

	wg.Wait()
}

func getRep(url string) (*RawResp, error) {
	var resp RawResp
	req, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer req.Body.Close()
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

type RawResp struct {
	Channel struct {
		Status string `json:"status"`
		Game   struct {
			Round                           int `json:"round"`
			BettingWindowTime               int `json:"bettingWindowTime"`
			BettingWindowPercentageComplete int `json:"bettingWindowPercentageComplete"`
			GameData                        struct {
				Objects []struct {
					Description string `json:"description"`
					Status      string `json:"status"`
					Properties  []struct {
						Name  string `json:"name"`
						Value string `json:"value"`
					} `json:"properties"`
					Name string `json:"name"`
				} `json:"objects"`
			} `json:"gameData"`
			Markets struct {
				Markets []struct {
					Status         string  `json:"status"`
					CommissionRate float64 `json:"commissionRate"`
					MarketType     string  `json:"marketType"`
					Selections     struct {
						Selections []struct {
							Name     string `json:"name"`
							Resource struct {
								Href         string `json:"href"`
								Title        string `json:"title"`
								ResponseType string `json:"responseType"`
							} `json:"resource"`
							Status                    string  `json:"status"`
							AmountMatched             float64 `json:"amountMatched"`
							BestAvailableToBackPrices struct {
								Prices []struct {
									Value           float64 `json:"value"`
									AmountUnmatched float64 `json:"amountUnmatched"`
								} `json:"prices"`
							} `json:"bestAvailableToBackPrices"`
							BestAvailableToLayPrices struct {
								Prices []struct {
									Value           float64 `json:"value"`
									AmountUnmatched float64 `json:"amountUnmatched"`
								} `json:"prices"`
							} `json:"bestAvailableToLayPrices"`
							ID int `json:"id"`
						} `json:"selections"`
						Type string `json:"type"`
					} `json:"selections"`
					ID     int `json:"id"`
					NextID int `json:"nextId"`
				} `json:"markets"`
				Currency string `json:"currency"`
			} `json:"markets"`
			ID int `json:"id"`
		} `json:"game"`
		ID       int    `json:"id"`
		Name     string `json:"name"`
		GameType string `json:"gameType"`
	} `json:"channel"`
}

module pokerodds

go 1.12

require (
	github.com/ernestosuarez/itertools v0.0.0-20160926002703-030494752e01
	github.com/jessevdk/go-flags v1.4.1-0.20181221193153-c0795c8afcf4
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.3.0
)

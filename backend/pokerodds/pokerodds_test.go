package pokerodds

import (
	"testing"
)
import "github.com/stretchr/testify/assert"

func TestCalculate(t *testing.T) {
	testCases := []struct {
		board  []string
		hs     Hands
		result []float64
	}{
		{
			board:  []string{"3h", "Td", "Kc", "Jd"},
			hs:     Hands{{"Tc", "Jh"}, {"Ks", "7s"}},
			result: []float64{0.0, 0.82, 0.18},
		},
		{
			board:  []string{"3h", "Td", "Kc", "3d"},
			hs:     Hands{{"Tc", "Jh"}, {"Ks", "7s"}, {"Js", "Ac"}, {"3c", "2h"}},
			result: []float64{0.0, 0.05, 0.05, 0.1, 0.8},
		},
		{
			board:  []string{"3h", "5d", "4c", "Jd"},
			hs:     Hands{{"Tc", "Jh"}, {"Ks", "7s"}, {"Kd", "7d"}, {"3c", "2h"}},
			result: []float64{0.13, 0.5, 0.0, 0.23, 0.15},
		},
		{
			board:  []string{"3h", "5d", "4c"},
			hs:     Hands{{"Tc", "Jh"}, {"Ks", "7s"}, {"Kd", "7d"}, {"3c", "2h"}},
			result: []float64{0.31, 0.15, 0.0, 0.05, 0.48},
		},
		{
			board:  []string{"8h", "2h", "4d"},
			hs:     Hands{{"Ks", "Th"}, {"2d", "7h"}, {"5s", "Ah"}, {"4h", "Ac"}},
			result: []float64{0.01, 0.17, 0.19, 0.28, 0.35},
		},
		// poker-odds Th4s 7h9d 2hAh 3dKc -b 9c9s5c -e
		//{
		//	board:  []string{"9c", "9s", "5c"},
		//	hs:     Hands{{"Th", "4s"}, {"7h", "9d"}, {"2h", "Ah"}, {"3d", "Kc"}},
		//	result: []float64{0.0, 0.04, 93.5, 1.3, 4.8},
		//},
	}

	calc := New()
	for _, tt := range testCases {
		assert.Equal(t, tt.result, calc.Calculate(tt.hs, tt.board))
	}
}

func TestPreprocessBoard(t *testing.T) {
	suitHistogram, histogram := preprocessBoard([]string{"3h", "Td", "Kc", "Jd", "As"})
	assert.Equal(t, []int{1, 1, 1, 2}, suitHistogram)
	assert.Equal(t, []int{1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0}, histogram)
}

func TestRankHand(t *testing.T) {
	testCases := []struct {
		name  string
		board []string
		hand  Hand
		rank  []int
	}{
		{
			name:  "Royal Flush",
			board: []string{"3h", "Th", "Kh", "Jh", "Tc"},
			hand:  Hand{"Qh", "Ah"},
			rank:  []int{9},
		},
		{
			name:  "Straight Flush",
			board: []string{"5h", "6h", "7h", "8h", "Tc"},
			hand:  Hand{"Qc", "9h"},
			rank:  []int{8, 9},
		},
		{
			name:  "Four Of a Kind",
			board: []string{"2h", "Qs", "Qh", "Qd", "Tc"},
			hand:  Hand{"Qc", "9h"},
			rank:  []int{7, 12, 10},
		},
		{
			name:  "Full House",
			board: []string{"3h", "Td", "Kc", "Jd", "Th"},
			hand:  Hand{"Tc", "Jh"},
			rank:  []int{6, 10, 11},
		},
		{
			name:  "Flush",
			board: []string{"5d", "8d", "Td", "Jd", "3h"},
			hand:  Hand{"2c", "Ad"},
			rank:  []int{5, 14, 11, 10, 8, 5},
		},
		{
			name:  "Straight",
			board: []string{"5c", "6h", "7d", "8s", "3h"},
			hand:  Hand{"9c", "Td"},
			rank:  []int{4, 10},
		},
		{
			name:  "Thee of a Kind",
			board: []string{"Td", "Th", "Tc", "2c", "3h"},
			hand:  Hand{"6c", "Jd"},
			rank:  []int{3, 10, 11, 6},
		},
		{
			name:  "Two Pair",
			board: []string{"3h", "Td", "Kc", "Jd", "As"},
			hand:  Hand{"Tc", "Jh"},
			rank:  []int{2, 11, 10, 14},
		},
		{
			name:  "Pair",
			board: []string{"3h", "Td", "Kc", "Jd", "As"},
			hand:  Hand{"Ks", "7s"},
			rank:  []int{1, 13, 14, 11, 10},
		},
		{
			name:  "High Card",
			board: []string{"3h", "Td", "2c", "Jd", "As"},
			hand:  Hand{"Ks", "7s"},
			rank:  []int{0, 14, 13, 11, 10, 7},
		},
	}
	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			suitHistogram, histogram := preprocessBoard(tt.board)
			rank := rankHand(tt.hand, tt.board, suitHistogram, histogram)
			assert.Equal(t, tt.rank, rank)
		})
	}
}

func TestFindWinner(t *testing.T) {
	testCases := []struct {
		board        []string
		hs           Hands
		winnersCount []int
	}{
		{
			board:        []string{"3h", "Td", "Kc", "Jd"},
			hs:           Hands{{"Tc", "Jh"}, {"Ks", "7s"}},
			winnersCount: []int{0, 36, 8},
		},
		{
			board:        []string{"3h", "Td", "Kc", "3d"},
			hs:           Hands{{"Tc", "Jh"}, {"Ks", "7s"}, {"Js", "Ac"}, {"3c", "2h"}},
			winnersCount: []int{0, 2, 2, 4, 32},
		},
	}
	for _, tt := range testCases {
		excludeCards := tt.board
		for _, cards := range tt.hs {
			excludeCards = append(excludeCards, cards...)
		}
		deck := createDeck(excludeCards)
		eb := generateExhaustiveBoards(deck, 1)
		winners := make([]int, len(tt.hs)+1)
		findWinner(tt.board, eb, tt.hs, winners)
		assert.Equal(t, tt.winnersCount, winners)
	}

}

func TestCompareHands(t *testing.T) {
	testCases := []struct {
		hands       [][]int
		winnerIndex int
	}{
		{hands: [][]int{{2, 11, 10, 13}, {2, 13, 3, 11}}, winnerIndex: 2},
	}
	for _, tt := range testCases {
		assert.Equal(t, tt.winnerIndex, compareHands(tt.hands))
	}
}

func TestGenerateExhaustiveBoards(t *testing.T) {
	testCases := []struct{
		deck []string
		boardLength int
		res [][]string
	}{
		{
			deck:[]string{"As", "Td", "2s", "3d", "4s", "5s"},
			boardLength:1,
			res:[][]string{{"As"}, {"Td"}, {"2s"}, {"3d"}, {"4s"}, {"5s"}},
		},
		{
			deck:[]string{"As", "Td", "2s"},
			boardLength:2,
			res:[][]string{{"As", "Td"}, {"As", "2s"}, {"Td", "2s"}},
		},
	}

	for _, tt := range testCases {
		eb := generateExhaustiveBoards(tt.deck, tt.boardLength)
		var res [][]string
		for card := range eb {
			res = append(res, card)
		}
		assert.Equal(t, tt.res, res)
	}
}

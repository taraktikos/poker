package pokerodds

import (
	"fmt"
	"github.com/ernestosuarez/itertools"
	"math"
	"sort"
)

// Ten, Jack, Queen, King, Ace
var cardValues = []string{"A", "K", "Q", "J", "T", "9", "8", "7", "6", "5", "4", "3", "2"}
var cardSuits = []string{spades, clubs, hearts, diamonds}
var ranks = []string{
	"High Card",
	"One Pair",
	"Two Pair",
	"Three of a Kind",
	"Straight",
	"Flush",
	"Full House",
	"Four of a Kind",
	"Straight Flush",
	"Royal Flush",
}
var Values = map[string]int{"2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9, "T": 10, "J": 11, "Q": 12, "K": 13, "A": 14}
var suitIndex = map[string]int{spades: 0, clubs: 1, hearts: 2, diamonds: 3}

const (
	clubs    = "c"
	diamonds = "d"
	hearts   = "h"
	spades   = "s"
)

type Hand []string
type Hands []Hand
type Board []string

type calc struct {
}

type card struct {
	name      string
	value     int
	suit      string
	suitIndex int
}

func New() *calc {
	return &calc{}
}

func (h Hands) cards() (cards []string) {
	for _, h := range h {
		cards = append(cards, h...)
	}
	return cards
}

func (c *calc) Calculate(h Hands, b Board) []float64 {
	cardsOnTable := append(b, h.cards()...)
	deck := createDeck(cardsOnTable)
	return runSimulation(h, b, deck)
}

func runSimulation(hs Hands, b Board, deck []string) []float64 {
	numPlayers := len(hs)
	eb := generateExhaustiveBoards(deck, 5-len(b))
	winners := make([]int, numPlayers+1)
	findWinner(b, eb, hs, winners)
	return winningPercentage(winners)
}

func winningPercentage(winners []int) (res []float64) {
	total := 0.0
	for _, w := range winners {
		total += float64(w)
	}
	for _, w := range winners {
		wp := math.Round((float64(w)/total)*100) / 100
		res = append(res, wp)
	}
	return res
}

func findWinner(b Board, eb <-chan []string, h Hands, winners []int) {
	result := make([][]int, len(h))
	for remainingBoard := range eb {
		fullBoard := append(b, remainingBoard...)
		suitHistogram, histogram := preprocessBoard(fullBoard)
		for i, elem := range h {
			result[i] = rankHand(elem, fullBoard, suitHistogram, histogram)
		}
		winnerIndex := compareHands(result)
		winners[winnerIndex]++
	}
}

func compareHands(rankedHands [][]int) int {
	winningIndex := 0
	bestHand := rankedHands[0]
	for i, rh := range rankedHands {
		if sliceEq(rh, bestHand) == 1 {
			bestHand = rh
			winningIndex = i
		}
	}
	for i := winningIndex + 1; i < len(rankedHands); i++ {
		if sliceEq(bestHand, rankedHands[i]) == 0 {
			return 0
		}
	}
	return winningIndex + 1
}

func sliceEq(a, b []int) int {
	if a == nil && b != nil {
		return 1
	}
	if a != nil && b == nil {
		return -1
	}
	longestArr := a
	if len(b) > len(longestArr) {
		longestArr = b
	}

	for i := range a {
		if i > len(a) {
			return 1
		}
		if i > len(b) {
			return -1
		}
		if a[i] < b[i] {
			return -1
		}
		if a[i] > b[i] {
			return 1
		}
	}
	return 0
}

//suitHistogram shows how often each card suit appears in the sequence
//histogram shows how often each card value appears in the sequence
func preprocessBoard(b []string) (suitHistogram []int, histogram []int) {
	suitHistogram = make([]int, 4)
	histogram = make([]int, 13)

	for _, card := range b {
		value := Values[string(card[0])]
		suitIndex := suitIndex[string(card[1])]
		histogram[14-value] += 1
		suitHistogram[suitIndex] += 1
	}
	return suitHistogram, histogram
}

// Two Pair: {2, high pair card, low pair card, kicker}
// Pair:     {1, pair card, kicker high card, kicker med card, kicker low card}
func rankHand(h Hand, b Board, suitHistogram []int, histogram []int) []int {
	maxSuit, pos := max(suitHistogram)
	if maxSuit >= 3 {
		flushIndex := pos
		for _, c := range h {
			suitIndex := suitIndex[string(c[1])]
			if suitIndex == flushIndex {
				maxSuit++
			}
		}
		if maxSuit >= 5 {
			flatBoard := append(b, h...)
			suitBoard := generateSuitBoard(flatBoard, flushIndex)
			isStraightFlush, highCard := detectStraightFlush(suitBoard)
			if isStraightFlush {
				if highCard == 14 {
					return []int{9}
				}
				return []int{8, highCard}
			}
			return append([]int{5}, getHighCards(suitBoard)...)
		}

	}
	fullHistogram := append([]int(nil), histogram...)
	for _, c := range h {
		value := Values[string(c[0])]
		fullHistogram[14-value]++
	}
	histogramBoard := preprocess(fullHistogram)
	var currentMax, currentMaxVal, secondMax, secondMaxVal int
	for _, item := range histogramBoard {
		val, frequency := item[0], item[1]
		if frequency > currentMax {
			secondMax, secondMaxVal = currentMax, currentMaxVal
			currentMax, currentMaxVal = frequency, val
		} else if frequency > secondMax {
			secondMax, secondMaxVal = frequency, val
		}
	}
	// Check if there is a Four Of a Kind
	if currentMax == 4 {
		return []int{7, currentMaxVal, detectHighestQuadKicker(histogramBoard)}
	}
	// Check if there is a Full House
	if currentMax == 3 && secondMax >= 2 {
		return []int{6, currentMaxVal, secondMaxVal}
	}
	// Check if there is a Straight
	if len(histogramBoard) >= 5 {
		straight, highCard := detectStraight(histogramBoard)
		if straight {
			return []int{4, highCard}
		}
	}
	// Check if there is a Three of a Kind
	if currentMax == 3 {
		return append([]int{3, currentMaxVal}, detectThreeOfKindKickers(histogramBoard)...)
	}
	// Pair or Two Pair
	if currentMax == 2 {
		// Check to see if there is a Two Pair
		if secondMax == 2 {
			return []int{2, currentMaxVal, secondMaxVal, detectHighestKicker(histogramBoard)}
		}
		// Return Pair
		return append([]int{1, currentMaxVal}, detectPairKicker(histogramBoard)...)
	}
	return append([]int{0}, getHighCards2(histogramBoard)...)
}

func getHighCards(histogram []int) []int {
	return histogram[:5]
}
func getHighCards2(histogram [][]int) (res []int) {
	for _, e := range histogram {
		res = append(res, e[0])
	}
	sort.Slice(res, func(i, j int) bool {
		return res[i] > res[j]
	})
	return res[:5]
}

func detectStraightFlush(suitBoard []int) (isStraightFlush bool, highCard int) {
	contiguousLength, failIndex := 1, len(suitBoard)-5
	for index, elem := range suitBoard {
		currentVal, nextVal := elem, suitBoard[index+1]
		if nextVal == currentVal-1 {
			contiguousLength++
			if contiguousLength == 5 {
				return true, currentVal + 3
			}
		} else {
			if index >= failIndex {
				if index == failIndex && nextVal == 5 && suitBoard[0] == 14 {
					return true, 5
				}
				break
			}
			contiguousLength = 1
		}
	}
	return false, -1
}

func detectStraight(histogram [][]int) (straight bool, highCard int) {
	contiguousLength, failIndex := 1, len(histogram)-5
	for index, elem := range histogram {
		currentVal, nextVal := elem[0], histogram[index+1][0]
		if nextVal == currentVal-1 {
			contiguousLength++
			if contiguousLength == 5 {
				return true, currentVal + 3
			}
		} else {
			if index >= failIndex {
				if index == failIndex && nextVal == 5 && histogram[0][0] == 14 {
					return true, 5
				}
				break
			}
			contiguousLength = 1
		}
	}
	return false, -1
}

func generateSuitBoard(b Board, flushIndex int) (histogram []int) {
	for _, c := range b {
		value := Values[string(c[0])]
		suitIndex := suitIndex[string(c[1])]
		if suitIndex == flushIndex {
			histogram = append(histogram, value)
		}
	}
	sort.Slice(histogram, func(i, j int) bool {
		return histogram[i] > histogram[j]
	})
	return histogram
}

//Returns slice of kickers: { kicker1, kicker2, kicker3 }
func detectPairKicker(histogram [][]int) []int {
	k1, k2 := -1, -1
	for _, elem := range histogram {
		if elem[1] != 2 {
			if k1 == -1 {
				k1 = elem[0]
			} else if k2 == -1 {
				k2 = elem[0]
			} else {
				return []int{k1, k2, elem[0]}
			}
		}
	}
	panic(fmt.Sprintf("can't detect PairKicker in %v", histogram))
}

func detectThreeOfKindKickers(histogram [][]int) []int {
	k1 := -1
	for _, elem := range histogram {
		if elem[1] != 3 {
			if k1 == -1 {
				k1 = elem[0]
			} else {
				return []int{k1, elem[0]}
			}
		}
	}
	panic(fmt.Sprintf("can't detect ThreeOfKindKickers in %v", histogram))
}

func detectHighestKicker(histogram [][]int) int {
	for _, elem := range histogram {
		if elem[1] == 1 {
			return elem[0]
		}
	}
	panic(fmt.Sprintf("can't detect HihestKicker in %v", histogram))
}

func detectHighestQuadKicker(histogram [][]int) int {
	for _, elem := range histogram {
		if elem[1] < 4 {
			return elem[0]
		}
	}
	panic(fmt.Sprintf("can't detect HighestQuadKicker in %v", histogram))
}

func preprocess(histogram []int) (res [][]int) {
	for i, v := range histogram {
		if v > 0 {
			res = append(res, []int{14 - i, v})
		}
	}
	return res
}

func generateExhaustiveBoards(deck []string, boardLength int) <-chan []string {
	return itertools.CombinationsStr(deck, boardLength)
}

func createDeck(withoutCards []string) (deck []string) {
	for _, s := range cardSuits {
		for _, c := range cardValues {
			card := c + s
			if !contains(withoutCards, card) {
				deck = append(deck, card)
			}
		}
	}
	return deck
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func max(s []int) (m int, p int) {
	p = 0
	m = s[p]
	for i, v := range s {
		if v > m {
			m = v
			p = i
		}
	}
	return m, p
}
